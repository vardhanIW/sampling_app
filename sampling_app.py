import os
import cv2
import json
import time
from datetime import datetime
import ffmpeg
import sys
import numpy as np
import requests
import base64

sampling_app_version = "comp.exe version = 1.7"

all_api_url = {}

def parse_api_detail_from_presampling():
    with open('pre_sampling_app.json', 'r') as openfile:
        json_object = json.load(openfile)

    if "APIDetails" in json_object.keys():
        a = {}
        for i in range(0, len(json_object["APIDetails"])):
            key = json_object["APIDetails"][i]["key"]
            value = json_object["APIDetails"][i]["value"]

            a[key] = value

        json_to_be_written = json.dumps(a, indent = 4)

        with open("api_detais.json", "w") as outfile:
            outfile.write(json_to_be_written)

        return True
    
    else:
        return False

def load_api_details():
    with open('api_detais.json', 'r') as openfile:
        json_object = json.load(openfile)

    global all_api_url
    all_api_url = json_object

def pull_api_details():
    with open('pre_sampling_app.json', 'r') as openfile:
        current_file = json.load(openfile)

    roid = current_file["SiteId"]

    try:
        headers = {

            'ApplicationId': '6eec1c22-26f4-4180-ae7f-f054de6d98d1', 
            'SessionToken': '0x01',
            'UserId': '634ea700-cbfe-4c99-b0b2-3b24a26f2d4a', 
            'CustId': '3226ab2e-c8ea-40ac-9c18-449155fef756',
            'MessageId': "test"
        }


        x = requests.get("https://silverlinemsprod.iwizardsolutions.com:8002/api/SamplingDvrAssetConfig?SiteId={}".format(roid),headers=headers)

        json_data = json.loads(x.text)
        
        payload_data = json.loads(json_data["RespBody"]["Payload"])
        payload_data = payload_data[0]
        #print(payload_data["APIDetails"])
        api_urls = {}
        for i in range(0, len(payload_data["APIDetails"])):
            key = payload_data["APIDetails"][i]["key"]
            value = payload_data["APIDetails"][i]["value"]

            api_urls[key] = value

        json_to_be_written = json.dumps(api_urls, indent = 4)

        with open("api_detais.json", "w") as outfile:
            outfile.write(json_to_be_written)
        #print(api_urls)
        return True
    except Exception as e:
        update_logs(str(e))
        return False
        
def save_to_cfg(sample_json):
    to_be_written = {}
    to_be_written["lastsynctime"] = sample_json["lastsynctime"]
    to_be_written["start_hour"] = sample_json["start_hour"]
    to_be_written["start_min"] = sample_json["start_min"]
    to_be_written["is_over_ride"] = sample_json["is_over_ride"]
    to_be_written["compressed_video_size"] = sample_json["compressed_video_size"]
    try:
        to_be_written["video_duration"] = int(sample_json["video_duration"])
        to_be_written["sample_gap_seconds"] = int(sample_json["sample_gap_seconds"])
    except:
        to_be_written["video_duration"] = 121
        to_be_written["sample_gap_seconds"] = 3600

    to_be_written["CameraDetail"] = []

    for i in range(0,len(sample_json["cameradetail"])):
        temp = {}
        temp["CameraName"] = sample_json["cameradetail"][i]["cameraname"]
        temp["area_threshoald"] = sample_json["cameradetail"][i]["area_threshold"]
        temp["frame_threshoald"] = sample_json["cameradetail"][i]["frame_threshold"]
        temp["Usecase_type"] = sample_json["cameradetail"][i]["usecase_type"]
        temp["camera_url"] = sample_json["cameradetail"][i]["camera_url"]
        temp["IsNewImagePresent"] = sample_json["cameradetail"][i]["isnewimagepresent"]
        temp["Referenceimages"] = sample_json["cameradetail"][i]["referenceimages"]

        all_roi = []

        for j in range(0, len(sample_json["cameradetail"][i]["roi"])):
            x = []
            y = []

            for z in range(0,len(sample_json["cameradetail"][i]["roi"][j])):
                x.append(sample_json["cameradetail"][i]["roi"][j][z]["x"])
                y.append(sample_json["cameradetail"][i]["roi"][j][z]["y"])

            x1 = min(x)
            y1 = min(y)
            x2 = max(x)
            y2 = max(y)

            all_roi.append([x1,y1,x2,y2])

        temp["roi"] = all_roi
        
        to_be_written["CameraDetail"].append(temp)

    return to_be_written
    

def compress_video(video_full_path, output_file_name, target_size):

    probe = ffmpeg.probe(video_full_path)
    # Video duration, in s.
    duration = float(probe['format']['duration'])

    # Target total bitrate, in bps.
    target_total_bitrate = (target_size * 1024 * 8) / (1.073741824 * duration)

    # Target video bitrate, in bps.
    video_bitrate = target_total_bitrate

    i = ffmpeg.input(video_full_path)
    ffmpeg.output(i, os.devnull,
                **{'c:v': 'libx264', 'b:v': video_bitrate, 'pass': 1, 'f': 'mp4'}
                ).overwrite_output().run()
    ffmpeg.output(i, output_file_name,
                **{'c:v': 'libx264', 'b:v': video_bitrate, 'pass': 2, 'c:a': 'aac'}
                ).overwrite_output().run()

    
def update_logs(text_to_add):
    try:
        if not os.path.exists("logs.txt"):
            mf = open("logs.txt", "w")
            mf.close()

        mf = open("logs.txt", "r")
        current_text = mf.read()
        mf.close()

        current_text = current_text + text_to_add + " ---- " +  str(datetime.now())+" ##" +"\n"

        mf = open("logs.txt", "w")
        mf.write(current_text)
        mf.close()
        
        if os.path.getsize("logs.txt") > 3000000:
            print("deleting logs")
            os.remove("logs.txt")
    except:
        os.remove("logs.txt")

def upload_images(cam_name, image_name,roid):
    global all_api_url
    try:
        print("sending data @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
        
        curr_time = str(datetime.now())
        curr_time =  curr_time.replace("-","").replace(":","").replace(" ","").split(".")[0]
        with open(image_name,'rb') as image_file: 
            encoded_string = base64.b64encode(image_file.read())
            b64 = encoded_string.decode("utf-8")
            MessageID = 'add_image_data'

            headers = {

                'ApplicationId': '6eec1c22-26f4-4180-ae7f-f054de6d98d1', 
                'SessionToken': '0x01',
                'UserId': '634ea700-cbfe-4c99-b0b2-3b24a26f2d4a', 
                'CustId': '3226ab2e-c8ea-40ac-9c18-449155fef756',
                'MessageId': MessageID # change
            }

            data = {         
            
                "FileName": curr_time + roid + image_name,
                "Base64": b64,
                "ContainerName": "hpcl"
            }

            end_point = all_api_url["image_upload_blob_url"]

            response = requests.put(end_point, headers=headers, data=data)
            #print(response.content)
            x = response.json()
            res = json.loads(x)
            print(res["FileURI"])


            data = {
                    "ImageURL": res["FileURI"],
                    "CameraName": cam_name,
                    "SiteId":roid
                    }

            f = all_api_url["update_image_to_backend"]
            response = requests.put(f, headers=headers, data=data)
            print(response.content)
            #print(response.content.decode("utf-8") )
            print('#################')
      
    except Exception as e:
        a = str(e)

def send_logs(roid):
    global all_api_url
    update_logs("sending logs")
    mf = open("logs.txt", "r")
    current_text = mf.read()
    mf.close()

    if len(current_text) > 0:
        try:
            curr_time = str(datetime.now())
            curr_time = curr_time.replace("-","")
            curr_time = curr_time.replace(" ","")
            curr_time = curr_time.replace(":","")
            curr_time = curr_time.replace(".","")
            unique_id = roid + curr_time

            headers = {

                        'ApplicationId': '6eec1c22-26f4-4180-ae7f-f054de6d98d1', 
                        'SessionToken': 'db6406fa-b175-43e2-a6fe-545c724a6a62',
                        'UserId': '634ea700-cbfe-4c99-b0b2-3b24a26f2d4a', 
                        'CustId': '3226ab2e-c8ea-40ac-9c18-449155fef756',
                        'MessageId': "logs_data"
                    }

            data  =  {
                    "ErrorCode":"OK",
                    "Message":current_text,
                    "CustomerId":"3226ab2e-c8ea-40ac-9c18-449155fef756",
                    "SiteId":roid,
                    "Timestamp":datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S'),
                    "CameraId":"all_logs",
                    "_id":unique_id
                    }

            end_point = all_api_url["logs_update_url"]

            response = requests.post(end_point, headers=headers, data=data)

            res_content = response.content.decode("utf-8")
            print('#################')
            update_logs(res_content)

            mf = open("logs.txt", "w")
            mf.close()

        except Exception as e:
            a = str(e)
            update_logs(a)

    else:
        update_logs("nothing in logs")

def downloaded_latest_app():
    global all_api_url
    try:
        headers = {

                'ApplicationId': '6eec1c22-26f4-4180-ae7f-f054de6d98d1', 
                'SessionToken': 'db6406fa-b175-43e2-a6fe-545c724a6a62',
                'UserId': '634ea700-cbfe-4c99-b0b2-3b24a26f2d4a', 
                'CustId': '3226ab2e-c8ea-40ac-9c18-449155fef756',
                'MessageId': "abcd"
            }
        x = requests.get(all_api_url["new_app_download"],headers=headers)
        json_data = json.loads(x.text)
        print(json_data)
        print("###"*10)
        payload_data = json.loads(json_data["RespBody"]["Payload"])
        print(type(payload_data))
        size_of_exe = payload_data.split(" ")[-1]
        payload_data = payload_data.split(" ")[0]
        

        # if "linux" in sys.platform:
        #     comand =  "wget "+ "-O comp_new.exe "+ payload_data
        #     os.system('timeout 10s {}'.format(comand))
            
        # else:
        #     comand = "curl "+ payload_data + " --output comp_new.exe"           
        #     os.system(comand)
        recieve = requests.get(payload_data)
        print("################# done")

        with open('comp_new.exe','wb') as f:
            f.write(recieve.content)

        b = os.path.getsize("comp_new.exe")

        if str(b) == size_of_exe:
            return True

        else:
            update_logs("app size not matching")
            update_logs("{} {}".format(b,size_of_exe))
            return False
        

    except Exception as e:
        a = str(e)
        update_logs(a)
        return False

def send_app_update_status(roid, updated_version):
    global all_api_url
    try:
        update_logs("updating app version for site on backend")
        headers = {

                'ApplicationId': '6eec1c22-26f4-4180-ae7f-f054de6d98d1', 
                'SessionToken': 'db6406fa-b175-43e2-a6fe-545c724a6a62',
                'UserId': '634ea700-cbfe-4c99-b0b2-3b24a26f2d4a', 
                'CustId': '3226ab2e-c8ea-40ac-9c18-449155fef756',
                'MessageId': "abcd"
            }

        data = {
                "SiteId": roid,
                "CustomerId": "3226ab2e-c8ea-40ac-9c18-449155fef756",
                "Timestamp": datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S'),
                "Version": updated_version,
                "Message": "OK",
                "Status": "OK"
                }

        end_point = all_api_url["app_upadte_info_to_backend"]

        response = requests.post(end_point, headers=headers, data=data)
        a = response.content.decode("utf-8")
        update_logs(a)
    
    except Exception as e:
        update_logs(str(e))

def check_app_update(roid):
    global all_api_url
    update_logs("checking for update")
    mf = open("app_version.txt", "r")
    current_version = mf.readlines()
    mf.close()
    current_version = current_version[0].split(" ")[-1]

    try:
        headers = {

                'ApplicationId': '6eec1c22-26f4-4180-ae7f-f054de6d98d1', 
                'SessionToken': 'db6406fa-b175-43e2-a6fe-545c724a6a62',
                'UserId': '634ea700-cbfe-4c99-b0b2-3b24a26f2d4a', 
                'CustId': '3226ab2e-c8ea-40ac-9c18-449155fef756',
                'MessageId': "abcd"
            }
        x = requests.get(all_api_url["check_site_for_update_app"].format(roid), headers=headers)
        json_data = json.loads(x.text)
        print(json_data)
        print("###"*10)
        payload_data = json.loads(json_data["RespBody"]["Payload"])

        print(payload_data)
        print(payload_data["UpgradeEnabled"])
        print(payload_data["Version"])

        latest_version = payload_data["Version"].split(" ")[-1]

        if latest_version != current_version and payload_data["UpgradeEnabled"]:
            update_logs("updated app available")
            download_flag = downloaded_latest_app()

            if download_flag:
                update_logs("download successful")
                if os.path.exists("comp_old.exe"):
                    os.remove("comp_old.exe")
                os.rename("comp.exe", "comp_old.exe")
                os.rename("comp_new.exe", "comp.exe")
                mf = open("app_version.txt", "w")
                mf.write(payload_data["Version"])
                mf.close()
                update_logs("app upade successful")
                send_app_update_status(roid, payload_data["Version"])

                with open('system_config.json', 'r') as openfile:
                    json_object = json.load(openfile)

                json_object["lastsynctime"] = 0
                json_object["is_over_ride"] = True
                json_object["start_hour"] = 0
                json_object["start_min"] = 1

                json_to_be_written = json.dumps(json_object, indent = 4)

                with open("system_config.json", "w") as outfile:
                    outfile.write(json_to_be_written)
                    
                update_logs("exiting now")
                sys.exit()
            else:
                update_logs("download failed")

                if os.path.exists("comp_new.exe"):
                    os.remove("comp_new.exe")

        else:
            update_logs("app is up to date")

    except Exception as e:
        a = str(e)
        update_logs(a)


class sampling:

    def __init__(self):
        
        update_logs("app restarted")

        if not os.path.exists("videos_to_upload.json"):
            vid = {"all_videos": []}
            to_write = json.dumps(vid, indent = 4)

            with open("videos_to_upload.json", "w") as outfile:
                outfile.write(to_write)

        mf = open("ROID.txt")
        self.roid = mf.readlines()[0]

        self.roid = self.roid.replace(" ", "")
        self.roid = self.roid.rstrip()

        #print(len(self.roid))
        print(repr(self.roid))
            
        with open('system_config.json', 'r') as openfile:
            json_object = json.load(openfile)
            #print(json_object)
        
        self.start_hour = json_object["start_hour"]
        self.start_min = json_object["start_min"]
        self.over_ride_system_config = json_object["is_over_ride"]
        self.over_ride_camera_config = False
        self.video_compressed_size = json_object["compressed_video_size"]
        self.last_sync = json_object["lastsynctime"]
        self.video_duration = json_object["video_duration"]
        self.sample_gap = json_object["sample_gap_seconds"]

        self.cameras = []
        self.current_camera = []
        self.decantation_camera = []
        
        self.is_running = False
        self.last_update = 0

        self.current_index = -1
        self.singe_camera_time = 0

        self.out = "None"
        self.vid_name = "a.avi"

        self.frame_count = 0
        self.total_time = 0

        curr_time = str(datetime.now())
        self.today_date = int(curr_time.split("-")[2].split(" ")[0])

        self.last_reset_date = self.today_date

        self.per_sampling = pre_sampling_app()

        if os.path.exists("camera_config.json"):

            with open('camera_config.json', 'r') as openfile:
                current_file = json.load(openfile)

            for i in range(0, len(current_file["all_cameras"])):

                if "Decantation" in current_file["all_cameras"][i]["Usecase_type"]:
                    self.decantation_camera.append(current_file["all_cameras"][i])

                else:
                    self.cameras.append(current_file["all_cameras"][i])
            print(len(self.cameras))
            self.last_update = current_file["last_reset"]
            self.last_reset_date = current_file["last_reset_date"]
            self.over_ride_camera_config = current_file["is_over_ride"]

        else:
            to_be_written = {"all_cameras":[]}
            
            for i in range(0,len(json_object["CameraDetail"])):
                camera_dict = {}

                camera_dict["video_sent_flag"] = False
                camera_dict["last_video_sent"] = 0
                camera_dict["CameraName"] = json_object["CameraDetail"][i]["CameraName"]
                camera_dict["camera_url"] = json_object["CameraDetail"][i]["camera_url"]
                camera_dict["Referenceimages"] = json_object["CameraDetail"][i]["Referenceimages"]
                camera_dict["Usecase_type"] = json_object["CameraDetail"][i]["Usecase_type"]
                camera_dict["roi"] = json_object["CameraDetail"][i]["roi"]

                to_be_written["all_cameras"].append(camera_dict)

                if "Decantation" in json_object["CameraDetail"][i]["Usecase_type"]:
                    self.decantation_camera.append(camera_dict)

                else:
                    self.cameras.append(camera_dict)

            to_be_written["last_reset"] = 0
            to_be_written["last_reset_date"] = self.today_date
            to_be_written["is_over_ride"] = False

            json_to_be_written = json.dumps(to_be_written, indent = 4)

            with open("camera_config.json", "w") as outfile:
                outfile.write(json_to_be_written)
        
        send_logs(self.roid)
        check_app_update(self.roid)

    def hitapi(self): 
        global all_api_url
        update_logs("trying video upload")
        c_name = ""
        try:
            with open('videos_to_upload.json', 'r') as openfile:
                curr_videos = json.load(openfile)
            
            for i in range(0, len(curr_videos["all_videos"])):
                print("trying uploading video")

                if curr_videos["all_videos"][i][2] == False:
                    
                    fileName = curr_videos["all_videos"][i][0]
                    cam_name = curr_videos["all_videos"][i][1]

                    update_logs("got a video " + cam_name)
                    c_name = cam_name
                    curr_time = str(datetime.now())
                    curr_time =  curr_time.replace("-","").replace(":","").replace(" ","").split(".")[0]
                    with open(fileName,'rb') as image_file: 
                        encoded_string = base64.b64encode(image_file.read())
                        b64 = encoded_string.decode("utf-8")
                        MessageID = 'add_video_data'

                        headers = {

                            'ApplicationId': '6eec1c22-26f4-4180-ae7f-f054de6d98d1', 
                            'SessionToken': '0x01',
                            'UserId': '634ea700-cbfe-4c99-b0b2-3b24a26f2d4a', 
                            'CustId': '3226ab2e-c8ea-40ac-9c18-449155fef756',
                            'MessageId': MessageID # change
                        }

                        data = {         
                        
                            "FileName": curr_time + self.roid + fileName,
                            "Base64": b64,
                            "ContainerName": "hpcl"
                        }

                        end_point = all_api_url["image_upload_blob_url"]

                        response = requests.put(end_point, headers=headers, data=data)
                        #print(response.content)
                        x = response.json()
                        res = json.loads(x)
                        print(res["FileURI"])
                        update_logs("video uploaded successfuly")

                        data = {
                                "VideoURL": res["FileURI"],
                                "CameraId": cam_name,
                                "SiteId": self.roid
                            }

                        f = all_api_url["update_video_to_backend"]
                        response = requests.post(f, headers=headers, data=data)
                        print(response.content)
                        #print(response.content.decode("utf-8") )
                        res_content = response.content.decode("utf-8")
                        print('#################')
                        update_logs(res_content)
                        update_logs("video updated on backend")
                        curr_videos["all_videos"][i][2] = True

                        json_to_written = json.dumps(curr_videos, indent = 4)
                        with open("videos_to_upload.json", "w") as outfile:
                            outfile.write(json_to_written)
                
        except Exception as e:
            a = str(e)
            print(type(a))
            print(a)
            update_logs("SA-EXCEPTION----VIDEO----" + c_name + "----" + a)
            #update_logs("VIDEO UPLOAD FAILED ---- " + a)
            pass
            
    def upadte_image(self, rtsp, cam_name):
        update_logs("new image required for " + cam_name)
        cap = cv2.VideoCapture(rtsp)
        flag = False
        for i in range(0, 20):
            ret, frame = cap.read()
            if not ret:
                update_logs(cam_name + " not accessable")
                continue
            else:
                if os.path.exists(cam_name + ".jpg"):
                    os.remove(cam_name + ".jpg")
                cv2.imwrite(cam_name + ".jpg", frame)
                flag = True

        if flag:
            update_logs("uploading new image for "+cam_name)
            upload_images(cam_name, cam_name + ".jpg",self.roid)

        
    def upload_new_images(self,cfg):
        update_logs("checking if new image required")
        for i in range(0, len(cfg["CameraDetail"])):
            if cfg["CameraDetail"][i]["IsNewImagePresent"] == True:
                self.upadte_image(cfg["CameraDetail"][i]["camera_url"], cfg["CameraDetail"][i]["CameraName"])


    def pull_system_config(self):
        global all_api_url
        try:
            update_logs("checking for new system config")
            headers = {

                'ApplicationId': '6eec1c22-26f4-4180-ae7f-f054de6d98d1', 
                'SessionToken': '0x01',
                'UserId': '634ea700-cbfe-4c99-b0b2-3b24a26f2d4a', 
                'CustId': '3226ab2e-c8ea-40ac-9c18-449155fef756',
                'MessageId': "test"
            }


            x = requests.get(all_api_url["for_pulling_system_config"].format(self.roid),headers=headers)

            json_data = json.loads(x.text)
            print(json_data)
            payload_data = json.loads(json_data["RespBody"]["Payload"])

            if self.last_sync != payload_data[0]["lastsynctime"]:
                to_be_written = save_to_cfg(payload_data[0])
                self.upload_new_images(to_be_written)
                json_to_be_written = json.dumps(to_be_written, indent = 4)

                with open("system_config.json", "w") as outfile:
                    outfile.write(json_to_be_written)
                
                os.remove("videos_to_upload.json")
                os.remove("camera_config.json")
                update_logs("got new system config")
                return True

        except Exception as e:
            a = str(e)
            update_logs(a)

        return False


    def check_date_time(self):
        curr_time = str(datetime.now())
        curr_time = curr_time.split(" ")[1]

        curr_hour = int(curr_time.split(":")[0])
        curr_min = int(curr_time.split(":")[1])
        date_curr = int(str(datetime.now()).split("-")[2].split(" ")[0])

        if self.last_reset_date != date_curr:

            if curr_hour >= self.start_hour:

                if curr_hour == self.start_hour:

                    if curr_min > self.start_min:
                        return True
                
                    else:
                        return False
                else:
                    return True 
        
        return False

    def run_over_ride(self):

        last_api_hit = time.time()
        last_cnfig_update = time.time()
        update_logs("running over ride")
        send_logs_timer = time.time()
        app_update_check_timer = time.time()
        decantation_timer = time.time()
        while True:

            if time.time()- last_api_hit > 120:
                self.hitapi()
                last_api_hit = time.time()

            if time.time() - decantation_timer > 300:
                self.decntation_algo()
                decantation_timer = time.time()

            if time.time()- app_update_check_timer > 3600:
                check_app_update(self.roid)
                app_update_check_timer = time.time()

            if time.time() - send_logs_timer > 600:
                send_logs(self.roid)
                send_logs_timer = time.time()

            if time.time() - last_cnfig_update > 600:
                last_cnfig_update = time.time()
                self.per_sampling.run_once()
                if self.pull_system_config():
                    return False

            if self.check_date_time() or time.time() - self.last_update > self.sample_gap:
                

                with open('camera_config.json', 'r') as openfile:
                    current_file = json.load(openfile)

                current_file["last_reset"] = time.time()

                if self.check_date_time():
                    current_file["last_reset_date"] = int(str(datetime.now()).split("-")[2].split(" ")[0])

                for i in range(0,len(current_file["all_cameras"])):
                    if current_file["all_cameras"][i]["video_sent_flag"]:
                        update_logs("SA-REPORT----"+current_file["all_cameras"][i]["CameraName"]+"----"+"UPLOADED")
                    else:
                        update_logs("SA-REPORT----"+current_file["all_cameras"][i]["CameraName"]+"----"+"NOT_UPLOADED")
                    current_file["all_cameras"][i]["video_sent_flag"] = False

                json_to_be_written = json.dumps(current_file, indent = 4)
                with open("camera_config.json", "w") as outfile:
                    outfile.write(json_to_be_written)

                for i in range(0,len(self.cameras)):
                    self.cameras[i]["video_sent_flag"] = False

                for i in range(0,len(self.decantation_camera)):
                    self.decantation_camera[i]["video_sent_flag"] = False

                self.is_running = True
                self.last_update = time.time()
                self.current_index = -1
                self.singe_camera_time = time.time()

                if self.check_date_time():
                    self.last_reset_date = int(str(datetime.now()).split("-")[2].split(" ")[0])

                vid = {"all_videos": []}
                to_write = json.dumps(vid, indent = 4)
                
                with open("videos_to_upload.json", "w") as outfile:
                    outfile.write(to_write)
            
            for i in range(0, len(self.cameras)):

                if self.cameras[i]["video_sent_flag"] == False:
                    print("over riding {}".format(i))
                    update_logs("over riding " + self.cameras[i]["CameraName"])
                    cap = cv2.VideoCapture(self.cameras[i]["camera_url"])
                    self.vid_name = 'to_be_compresed_{}.avi'.format(self.cameras[i]["CameraName"])
                    self.out = cv2.VideoWriter(self.vid_name ,cv2.VideoWriter_fourcc('M','J','P','G'), 5,(1280,720))
                    video_timer = time.time()
                    is_saved = False
                    skiping_frames = time.time()
                    while time.time() - video_timer < self.video_duration:
                        ret , frame = cap.read()

                        if not ret:
                            update_logs("SA-EXCEPTION----CAMERA----" + self.cameras[i]["CameraName"] + "----not working")
                            update_logs(self.cameras[i]["camera_url"] + "not working")
                            time.sleep(5)
                            continue

                        if time.time()-skiping_frames > .32:
                            frame = cv2.resize(frame, (1280,720))
                            self.out.write(frame)
                            skiping_frames = time.time()
                            is_saved = True

                    cap.release()
                    self.out.release()

                    if is_saved:
                        try:
                            print("compressing")
                            update_logs("saving done starting compression")
                            compress_video(self.vid_name, self.cameras[i]["CameraName"]+'.mkv', self.video_compressed_size)
                            update_logs("compression done")

                            with open('videos_to_upload.json', 'r') as openfile:
                                curr_videos = json.load(openfile)
                            
                            curr_videos["all_videos"]. append([self.cameras[i]["CameraName"]+'.mkv', self.cameras[i]["CameraName"], False])

                            json_to_written = json.dumps(curr_videos, indent = 4)

                            with open("videos_to_upload.json", "w") as outfile:
                                outfile.write(json_to_written)

                            #self.hitapi(self.cameras[i]["CameraName"]+'.mkv', self.cameras[i]["CameraName"])
                            
                            self.cameras[i]["video_sent_flag"] = True

                            with open('camera_config.json', 'r') as openfile:
                                current_file = json.load(openfile)

                            for j in range(0,len(current_file["all_cameras"])):
                                if current_file["all_cameras"][j]["CameraName"] == self.cameras[i]["CameraName"]:
                                    current_file["all_cameras"][j]["video_sent_flag"] = True

                            json_to_be_written = json.dumps(current_file, indent = 4)
                            with open("camera_config.json", "w") as outfile:
                                outfile.write(json_to_be_written)
                        except Exception as e:
                            a = str(e)
                            update_logs(a)

            time.sleep(60)      

    def decantation_varification(self,roi):
        global all_api_url
        try:
            update_logs("varifing decantation image")
            image = "decantation_varification_image.jpg"

            with open(image, "rb") as image_file:
                encoded_string = base64.b64encode(image_file.read())
                base64image = encoded_string.decode("utf-8")

            data = {"Base64":base64image,"roi":roi,"ro_id":self.roid}

            # print(data)
            f = all_api_url["decantation_check_url"]

            res = requests.post(f, data=json.dumps(data),timeout=10)
            print(res.content)
            data = json.loads(res.content)
            print("Responce is ", data)
            # return data 
            #  {'Is_Truck_Present': False} // True

            return data["Is_Truck_Present"]

        except Exception as e: 
            a = str(e)      
            update_logs(a)
            return False    


    def decntation_algo(self):
        update_logs("checking dcantation camera")
        if len(self.decantation_camera) > 0:
            
            for i in range(0, len(self.decantation_camera)):
                
                if self.decantation_camera[i]["video_sent_flag"] == False:
                    update_logs("running decantation cmaera "+ str(i))

                    cap = cv2.VideoCapture(self.decantation_camera[i]["camera_url"])

                    x1 = int(self.decantation_camera[i]["roi"][0][0] * 1280)
                    x2 = int(self.decantation_camera[i]["roi"][0][2] * 1280)
                    y1 = int(self.decantation_camera[i]["roi"][0][1] * 720)
                    y2 = int(self.decantation_camera[i]["roi"][0][3] * 720)
                    roi = [x1,y1,x2,y2]

                    for j in range(0,10):
                        ret, frame = cap.read()

                    if not ret:
                        update_logs("decantation camera nor working " + self.decantation_camera[i]["camera_url"])
                        break
                    
                    frame = cv2.resize(frame, (1280,720))
                    cv2.imwrite("decantation_varification_image.jpg", frame)

                    flag = self.decantation_varification(roi)

                    if flag:
                        update_logs("first check passes")
                        cap.release()
                        time.sleep(50)
                        cap = cv2.VideoCapture(self.decantation_camera[i]["camera_url"])

                        for j in range(0,10):
                            ret, frame = cap.read()

                        if not ret:
                            update_logs("decantation camera nor working " + self.decantation_camera[i]["camera_url"])
                            break
                        
                        frame = cv2.resize(frame, (1280,720))
                        cv2.imwrite("decantation_varification_image.jpg", frame)

                        flag = self.decantation_varification(roi)

                        if flag:
                            update_logs("second check passed")
                            cap.release()
                            time.sleep(600)
                            cap = cv2.VideoCapture(self.decantation_camera[i]["camera_url"])

                            for j in range(0,10):
                                ret, frame = cap.read()

                            if not ret:
                                update_logs("decantation camera nor working " + self.decantation_camera[i]["camera_url"])
                                break
                            
                            frame = cv2.resize(frame, (1280,720))
                            cv2.imwrite("decantation_varification_image.jpg", frame)
                            cap.release()
                            flag = self.decantation_varification(roi)

                            if flag:
                                update_logs("saving decantation video")
                                self.vid_name = 'to_be_compresed_{}.avi'.format(self.decantation_camera[i]["CameraName"])
                                self.out = cv2.VideoWriter(self.vid_name ,cv2.VideoWriter_fourcc('M','J','P','G'), 5,(1280,720))
                                cap = cv2.VideoCapture(self.decantation_camera[i]["camera_url"])
                                start_time = time.time()
                                skiping_frames =time.time()
                                is_saved_success = False
                                while time.time()-start_time < self.video_duration:
                                    #time.sleep(1)
                                    ret , frame = cap.read()

                                    if not ret:
                                        update_logs("SA-EXCEPTION----CAMERA----" + self.decantation_camera[i]["CameraName"] + "----not working")
                                        update_logs(self.decantation_camera[i]["camera_url"] + "not working")
                                        #is_saved_success = False
                                        continue

                                    if time.time()-skiping_frames > .32:
                                        is_saved_success = True
                                        frame = cv2.resize(frame, (1280,720))
                                        self.out.write(frame)
                                        skiping_frames = time.time()
                                
                                cap.release()
                                self.out.release()

                                if is_saved_success:
                                    update_logs("starting video compression")
                                    compress_video(self.vid_name, self.decantation_camera[i]["CameraName"]+'.mkv', self.video_compressed_size)
                                    update_logs("video compression done")
                                    with open('videos_to_upload.json', 'r') as openfile:
                                        curr_videos = json.load(openfile)
                                    
                                    curr_videos["all_videos"]. append([self.decantation_camera[i]["CameraName"]+'.mkv', self.decantation_camera[i]["CameraName"], False])

                                    json_to_written = json.dumps(curr_videos, indent = 4)

                                    with open("videos_to_upload.json", "w") as outfile:
                                        outfile.write(json_to_written)

                                    self.decantation_camera[i]["video_sent_flag"] = True

                                    with open('camera_config.json', 'r') as openfile:
                                        current_file = json.load(openfile)

                                    for k in range(0,len(current_file["all_cameras"])):
                                        if current_file["all_cameras"][k]["CameraName"] == self.decantation_camera[i]["CameraName"]:
                                            current_file["all_cameras"][k]["video_sent_flag"] = True

                                    json_to_be_written = json.dumps(current_file, indent = 4)
                                    with open("camera_config.json", "w") as outfile:
                                        outfile.write(json_to_be_written)



    def run(self):
        global all_api_url
        update_logs("inside run")
        curr_time = str(datetime.now())
        curr_time = curr_time.split(" ")[1]

        curr_hour = int(curr_time.split(":")[0])
        curr_min = int(curr_time.split(":")[1])

        print(curr_hour , curr_min)

        if self.last_update == 0:

            while self.last_update == 0:
                curr_time = str(datetime.now())
                curr_time = curr_time.split(" ")[1]

                curr_hour = int(curr_time.split(":")[0])
                curr_min = int(curr_time.split(":")[1])

                if curr_hour >= self.start_hour:

                    if curr_hour == self.start_hour:

                        if curr_min > self.start_min:
                            self.last_update = time.time()

                            with open('camera_config.json', 'r') as openfile:
                                current_file = json.load(openfile)

                            for i in range(0,len(current_file["all_cameras"])):
                                current_file["all_cameras"][i]["video_sent_flag"] = False

                            current_file["last_reset"] = time.time()

                            json_to_be_written = json.dumps(current_file, indent = 4)
                            with open("camera_config.json", "w") as outfile:
                                outfile.write(json_to_be_written)

                            self.is_running = True
                            print(curr_hour , self.start_hour)

                    else:
                        self.last_update = time.time()

                        with open('camera_config.json', 'r') as openfile:
                            current_file = json.load(openfile)

                        for i in range(0,len(current_file["all_cameras"])):
                            current_file["all_cameras"][i]["video_sent_flag"] = False

                        current_file["last_reset"] = time.time()

                        json_to_be_written = json.dumps(current_file, indent = 4)
                        with open("camera_config.json", "w") as outfile:
                            outfile.write(json_to_be_written)

                        self.is_running = True


        last_api_hit = time.time()
        last_cnfig_update = time.time()
        send_logs_timer = time.time()
        app_update_check_timer = time.time()
        decantation_timer = time.time()

        while True:

            time.sleep(5)

            if time.time() - decantation_timer > 300:
                print("checking decantation ***********")
                self.decntation_algo()
                decantation_timer = time.time()

            if time.time()- app_update_check_timer > 3600:
                check_app_update(self.roid)
                app_update_check_timer = time.time()

            if time.time()- last_api_hit > 120:
                self.hitapi()
                last_api_hit = time.time()

            if time.time() - last_cnfig_update > 600:
                last_cnfig_update = time.time()
                self.per_sampling.run_once()
                if self.pull_system_config():
                    return False

            if time.time() - send_logs_timer > 600:
                send_logs(self.roid)
                send_logs_timer = time.time()
                
            if self.check_date_time() or time.time() - self.last_update > self.sample_gap:
                update_logs("resetting for next date")
                print("resetting for next date")
                with open('camera_config.json', 'r') as openfile:
                    current_file = json.load(openfile)

                current_file["last_reset"] = time.time()
                if self.check_date_time():
                    current_file["last_reset_date"] = int(str(datetime.now()).split("-")[2].split(" ")[0])
                for i in range(0,len(current_file["all_cameras"])):
                    if current_file["all_cameras"][i]["video_sent_flag"]:
                        update_logs("SA-REPORT----"+current_file["all_cameras"][i]["CameraName"]+"----"+"UPLOADED")
                    else:
                        update_logs("SA-REPORT----"+current_file["all_cameras"][i]["CameraName"]+"----"+"NOT_UPLOADED")
                    current_file["all_cameras"][i]["video_sent_flag"] = False

                json_to_be_written = json.dumps(current_file, indent = 4)
                with open("camera_config.json", "w") as outfile:
                    outfile.write(json_to_be_written)

                for i in range(0,len(self.cameras)):
                    self.cameras[i]["video_sent_flag"] = False

                for i in range(0,len(self.decantation_camera)):
                    self.decantation_camera[i]["video_sent_flag"] = False

                self.is_running = True
                self.last_update = time.time()
                self.current_index = -1
                self.singe_camera_time = time.time()
                if self.check_date_time():
                    self.last_reset_date = int(str(datetime.now()).split("-")[2].split(" ")[0])

                vid = {"all_videos": []}
                to_write = json.dumps(vid, indent = 4)
                
                with open("videos_to_upload.json", "w") as outfile:
                    outfile.write(to_write)

            
            if len(self.current_camera) == 0:

                for i in range(0, len(self.cameras)):
                    
                    if i != self.current_index:

                        if i > self.current_index or self.current_index ==len(self.cameras)-1:
                            if self.cameras[i]["video_sent_flag"] == False:
                                print("Starting Camera {}".format(i))
                                
                                self.current_camera.append(self.cameras[i])
                                self.current_index = i
                                self.singe_camera_time = time.time()
                                break

                if len(self.current_camera) == 0:

                    if self.cameras[self.current_index]["video_sent_flag"] == False:
                        self.current_camera.append(self.cameras[self.current_index])
                        print("Retarting Camera {}".format(self.current_index))
                        self.singe_camera_time = time.time()

                    else:
                        self.current_index = -1
                        self.singe_camera_time = time.time()
            
            else:
                cap = cv2.VideoCapture(self.current_camera[0]["camera_url"])
                update_logs("Starting Camera " + self.current_camera[0]["camera_url"])
                images = []
                roi = []
                difference_counter = {}
                if_save_flag = False
                is_first_frame = False
                skiping_frames = time.time()
                total_video_duration_timer = time.time()

                for i in range(0, len(self.current_camera[0]["Referenceimages"])):
                    img = cv2.imread(self.current_camera[0]["Referenceimages"][i])
                    img = cv2.resize(img, (1280,720))
                    images.append(img)
                
                for i in range(0, len(self.current_camera[0]["roi"])):
                    x1 = int(self.current_camera[0]["roi"][i][0] * 1280)
                    x2 = int(self.current_camera[0]["roi"][i][2] * 1280)
                    y1 = int(self.current_camera[0]["roi"][i][1] * 720)
                    y2 = int(self.current_camera[0]["roi"][i][3] * 720)

                    roi.append([x1,y1,x2,y2])
                    difference_counter[str(i)] = 0

                while time.time() - self.singe_camera_time < 600 or if_save_flag:
                    start_time = time.time()
                    ret, frame = cap.read()

                    if not ret:
                        update_logs("SA-EXCEPTION----CAMERA----" + self.current_camera[0]["CameraName"] + "----not working")

                        update_logs("camera not working " + self.current_camera[0]["camera_url"])
                        time.sleep(5)
                        continue
                    
                    frame = cv2.resize(frame, (1280,720))

                    if not if_save_flag:
                        sub_image = "None"

                        for i in range(0, len(images)):
                            difference = cv2.subtract(images[i], frame)
                            Conv_hsv_Gray = cv2.cvtColor(difference, cv2.COLOR_BGR2GRAY)
                            (T, thresh) = cv2.threshold(Conv_hsv_Gray, 30, 255,cv2.THRESH_BINARY)

                            if type(sub_image) == type(thresh):
                                sub_image = cv2.bitwise_and(sub_image, thresh)

                            else:
                                sub_image = thresh

                        for i in range(0,len(roi)):

                            croped_frame = sub_image[roi[i][1]:roi[i][3],roi[i][0]:roi[i][2]]
                            total_number = np.count_nonzero(croped_frame)

                            if total_number/((roi[i][2]-roi[i][0]) * (roi[i][3]-roi[i][1])) > 0.4:
                                difference_counter[str(i)] += 1
                                print(difference_counter[str(i)])
                                update_logs("differe counter {}".format(difference_counter[str(i)]))
                                if difference_counter[str(i)] > 15:
                                    if_save_flag = True
                                    is_first_frame = True
                            else:
                                difference_counter[str(i)] = 0
                        
                        self.total_time += time.time() - start_time
                        self.frame_count += 1  

                        if self.frame_count == 30:

                            if self.total_time > 45:
                                update_logs("system is slow updating to over ride")
                                with open('camera_config.json', 'r') as openfile:
                                    current_file = json.load(openfile)

                                current_file["is_over_ride"] = True

                                json_to_be_written = json.dumps(current_file, indent = 4)

                                with open("camera_config.json", "w") as outfile:
                                    outfile.write(json_to_be_written)

                                cap.release()
                                return True

                            self.total_time = 0
                            self.frame_count = 0
                    
                    else:
                        
                        if is_first_frame:
                            self.vid_name = 'to_be_compresed_{}.avi'.format(self.current_camera[0]["CameraName"])
                            self.out = cv2.VideoWriter(self.vid_name ,cv2.VideoWriter_fourcc('M','J','P','G'), 5,(1280,720))
                            is_first_frame = False
                            skiping_frames = time.time()
                            total_video_duration_timer = time.time()
                            print("start saving")

                        if time.time() - total_video_duration_timer < self.video_duration:
                            if time.time()-skiping_frames > .32:
                                self.out.write(frame)
                                skiping_frames = time.time()

                        else:
                            self.out.release()
                            #cap.release()
                            break
    
                cap.release()

                if if_save_flag:
                    update_logs("starting video compression")
                    compress_video(self.vid_name, self.current_camera[0]["CameraName"]+'.mkv', self.video_compressed_size)
                    update_logs("video compression done")
                    with open('videos_to_upload.json', 'r') as openfile:
                        curr_videos = json.load(openfile)
                    
                    curr_videos["all_videos"]. append([self.current_camera[0]["CameraName"]+'.mkv', self.current_camera[0]["CameraName"], False])

                    json_to_written = json.dumps(curr_videos, indent = 4)

                    with open("videos_to_upload.json", "w") as outfile:
                        outfile.write(json_to_written)

                    #self.hitapi(self.current_camera[0]["CameraName"]+'.mkv', self.current_camera[0]["CameraName"])
                    self.current_camera.pop()

                    self.cameras[self.current_index]["video_sent_flag"] = True

                    with open('camera_config.json', 'r') as openfile:
                        current_file = json.load(openfile)

                    for i in range(0,len(current_file["all_cameras"])):
                        if current_file["all_cameras"][i]["CameraName"] == self.cameras[self.current_index]["CameraName"]:
                            current_file["all_cameras"][i]["video_sent_flag"] = True

                    json_to_be_written = json.dumps(current_file, indent = 4)
                    with open("camera_config.json", "w") as outfile:
                        outfile.write(json_to_be_written)
                
                else:
                    self.current_camera.pop()


class pre_sampling_app:

    def __init__(self):

        update_logs("starting presampling")
        self.roid  = ""
        self.all_dvr = []
        self.all_cameras = []
        self.all_camera_details = []
        self.last_config_pulled_time = 0

        if os.path.exists("persampling_status.json"):   
            with open('persampling_status.json', 'r') as openfile:
                current_file = json.load(openfile)
            
            self.roid = current_file["SiteId"]
            self.all_cameras = current_file["all_cameras"]
            self.all_dvr = current_file["DVRInfoList"]
            self.all_camera_details = current_file["all_camer_current"]
            self.last_config_pulled_time = current_file["last_pulled"]

        else:
            with open('pre_sampling_app.json', 'r') as openfile:
                current_file = json.load(openfile)

            self.roid = current_file["SiteId"]

            to_write = {}

            mf = open("ROID.txt", "w")
            mf.write(self.roid + "\n")
            mf.close()

            for i in range(0,len(current_file["DVRInfoList"])):
                temp = current_file["DVRInfoList"][i]
                temp["is_checked"] = False
                temp["is_image_send"] = False
                temp["total_channels_checked"] = 0
                temp["last_checked_time"] = 0

                self.all_dvr.append(temp)

            to_write["DVRInfoList"] = self.all_dvr

            for i in range(0,len(current_file["all_cameras"])):
                temp = current_file["all_cameras"][i]
                temp["is_checked"] = False
                temp["is_image_send"] = False
                temp["last_checked_time"] = 0

                self.all_cameras.append(temp)

            to_write["all_cameras"] = self.all_cameras

            to_write["all_camer_current"] = self.all_camera_details
            to_write["last_pulled"] = time.time()
            to_write["SiteId"] = self.roid

            json_to_be_written = json.dumps(to_write, indent = 4)

            with open("persampling_status.json", "w") as outfile:
                outfile.write(json_to_be_written)

        send_logs(self.roid)

    def pull_system_config(self):
        global all_api_url
        try:
            update_logs("trying to get system config")
            headers = {

                'ApplicationId': '6eec1c22-26f4-4180-ae7f-f054de6d98d1', 
                'SessionToken': '0x01',
                'UserId': '634ea700-cbfe-4c99-b0b2-3b24a26f2d4a', 
                'CustId': '3226ab2e-c8ea-40ac-9c18-449155fef756',
                'MessageId': "test"
            }


            x = requests.get(all_api_url["for_pulling_system_config"].format(self.roid),headers=headers)

            json_data = json.loads(x.text)
            print(json_data)
            payload_data = json.loads(json_data["RespBody"]["Payload"])
            to_be_written = save_to_cfg(payload_data[0])

            json_to_be_written = json.dumps(to_be_written, indent = 4)
            with open("system_config.json", "w") as outfile:
                outfile.write(json_to_be_written)

            update_logs("updated new system config")
            return True

        except Exception as e:
            print(e)
            update_logs("no udateded system config found")
            return False

    def pull_pre_sampling_config(self):
        global all_api_url
        try:
            update_logs("pulling new camera details")
            headers = {

                'ApplicationId': '6eec1c22-26f4-4180-ae7f-f054de6d98d1', 
                'SessionToken': '0x01',
                'UserId': '634ea700-cbfe-4c99-b0b2-3b24a26f2d4a', 
                'CustId': '3226ab2e-c8ea-40ac-9c18-449155fef756',
                'MessageId': "test"
            }


            x = requests.get(all_api_url["for_pulling_presmapling_config"].format(self.roid),headers=headers)

            json_data = json.loads(x.text)
            print(json_data)
            payload_data = json.loads(json_data["RespBody"]["Payload"])
            payload_data = payload_data[0]
            api_urls = {}

            for i in range(0, len(payload_data["APIDetails"])):
                key = payload_data["APIDetails"][i]["key"]
                value = payload_data["APIDetails"][i]["value"]

                api_urls[key] = value

            if api_urls != all_api_url:
                update_logs("new api details available")
                json_to_be_written = json.dumps(api_urls, indent = 4)

                with open("api_detais.json", "w") as outfile:
                    outfile.write(json_to_be_written)
                
                all_api_url = api_urls
                update_logs("updated new api details")

            for i in range(0,len(payload_data["DVRInfoList"])):
                is_already_present = False

                for j in range(0,len(self.all_dvr)):
                    if self.all_dvr[j]["DVR_IP"] == payload_data["DVRInfoList"][i]["DVR_IP"] and self.all_dvr[j]["DVR_ID"] == payload_data["DVRInfoList"][i]["DVR_ID"] and self.all_dvr[j]["DVR_Password"] == payload_data["DVRInfoList"][i]["DVR_Password"] and self.all_dvr[j]["DVR_RTSP"] == payload_data["DVRInfoList"][i]["DVR_RTSP"]:
                        is_already_present = True
                        break

                if not is_already_present:
                    temp = payload_data["DVRInfoList"][i]
                    temp["is_checked"] = False
                    temp["is_image_send"] = False
                    temp["total_channels_checked"] = 0
                    temp["last_checked_time"] = 0

                    self.all_dvr.append(temp)

                    with open('persampling_status.json', 'r') as openfile:
                        current_file = json.load(openfile)

                    current_file["DVRInfoList"].append(temp)

                    json_to_be_written = json.dumps(current_file, indent = 4)

                    with open("persampling_status.json", "w") as outfile:
                        outfile.write(json_to_be_written)

                    print("new dvr **************************")
                    update_logs("got new cameras")
                    print(temp)
        
        except Exception as e:
            a = str(e)
            update_logs(a)
            print(e)


    def check_cameras(self):
        pass

    def upload_images(self,rtsp, cam_name, image_name):
        global all_api_url
        try:
            print("sending data @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@")
            update_logs("trying to upload image")
            curr_time = str(datetime.now())
            curr_time =  curr_time.replace("-","").replace(":","").replace(" ","").split(".")[0]
            with open(image_name,'rb') as image_file: 
                encoded_string = base64.b64encode(image_file.read())
                b64 = encoded_string.decode("utf-8")
                MessageID = 'add_image_data'

                headers = {

                    'ApplicationId': '6eec1c22-26f4-4180-ae7f-f054de6d98d1', 
                    'SessionToken': '0x01',
                    'UserId': '634ea700-cbfe-4c99-b0b2-3b24a26f2d4a', 
                    'CustId': '3226ab2e-c8ea-40ac-9c18-449155fef756',
                    'MessageId': MessageID # change
                }

                data = {         
                
                    "FileName": curr_time + self.roid + image_name,
                    "Base64": b64,
                    "ContainerName": "hpcl"
                }

                end_point = all_api_url["image_upload_blob_url"]

                response = requests.put(end_point, headers=headers, data=data)
                #print(response.content)
                x = response.json()
                res = json.loads(x)
                print(res["FileURI"])
                update_logs("image uploaded successful")

                data = {
                        "ImageURL": res["FileURI"],
                        "CameraName": cam_name,
                        "ImgName": image_name,
                        "SiteId":self.roid,
                        "RTSPURL":rtsp,
                        "timestamp": datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
                    }

                f = all_api_url["update_image_to_backend"]
                response = requests.post(f, headers=headers, data=data)
                print(response.content)
                #print(response.content.decode("utf-8") )
                print('#################')
                update_logs("data updated to servr")
        except Exception as e:
            a = str(e)
            update_logs(a)
            

    def check_dvrs(self):
        global all_api_url
        update_logs("checking dvrs")
        for i in range(0, len(self.all_dvr)):
            
            if self.all_dvr[i]["is_checked"] == False:
                dvr_url = self.all_dvr[i]["DVR_RTSP"]
                dvr_url = dvr_url.replace("<IP>", self.all_dvr[i]["DVR_IP"])

                id = self.all_dvr[i]["DVR_ID"]
                id = id.replace("@", "%40")

                password = self.all_dvr[i]["DVR_Password"]
                password = password.replace("@", "%40")
                password = password.replace("#", "%23")

                dvr_url = dvr_url.replace("<ID>", id)
                dvr_url = dvr_url.replace("<Password>", password)

                print(dvr_url)

                channel = 1

                faild_channels = []
                success_channels = []

                if "<Channel>" in dvr_url:

                    while True:
                        time.sleep(2)
                        url = dvr_url
                        url = url.replace("<Channel>" , str(channel))
                        update_logs("checking " + url)
                        cap = cv2.VideoCapture(url)
                        update_logs("source created")
                        for z in range(0,10):
                            ret, frame = cap.read()

                        if not ret:
                            update_logs("camera not accessable")
                            try:
                                is_pingable = False

                                if "linux" in sys.platform:
                                    res = os.system("ping -c 1 " + self.all_dvr[i]["DVR_IP"])

                                    #and then check the response...
                                    if res == 0:
                                        is_pingable = True
                                        update_logs("ip is pingable")
                                        
                                    else:
                                        update_logs("ip is not pingable")

                                else:
                                    res = os.popen("ping " + self.all_dvr[i]["DVR_IP"]).read()
                                    print("############")
                                    #print(res)
                                    res = str(res)
                                    #print(type(res))

                                    if "ms TTL=" in res:
                                        is_pingable = True
                                        update_logs("ip is pingable")
                                        update_logs(res)

                                    else:
                                        update_logs("ip is not pingable")
                                        update_logs(res)
                                        
                                if is_pingable:

                                    process = (
                                        ffmpeg
                                        .input(url)
                                        .output('-', format='h264')
                                        .run(capture_stdout=True, capture_stderr=True)
                                    )
                                else:
                                    try:
                                        try:
                                            headers = {

                                                'ApplicationId': '6eec1c22-26f4-4180-ae7f-f054de6d98d1', 
                                                'SessionToken': '0x01',
                                                'UserId': '634ea700-cbfe-4c99-b0b2-3b24a26f2d4a', 
                                                'CustId': '3226ab2e-c8ea-40ac-9c18-449155fef756',
                                                'MessageId': "test"
                                            }
                                            data = {
                                                "ROID": self.roid,
                                                "Message": "DVR_NOT_ON_THE_SAME_NETWORK",
                                                "MessageType": "DVR_NOT_ON_THE_SAME_NETWORK"
                                                }

                                            end_point = all_api_url["dvr_access_error_message"]

                                            response = requests.post(end_point, headers=headers, data=data)
                                            print(data)
                                        except:
                                            pass

                                        self.all_dvr[i]["is_checked"] = True
                                        update_logs("dvr check complete")
                                        with open('persampling_status.json', 'r') as openfile:
                                            current_file = json.load(openfile)

                                        current_file["DVRInfoList"] = self.all_dvr

                                        json_to_be_written = json.dumps(current_file, indent = 4)

                                        with open("persampling_status.json", "w") as outfile:
                                            outfile.write(json_to_be_written)

                                    except:
                                        pass

                                    break

                            except Exception as e:
                                time.sleep(3)
                                faild_channels.append(channel)

                                error_type = "UNKNOWN"
                                a = e.stderr.decode('utf8')
                                x = a.split("\n")[-3] + "\n"+a.split("\n")[-2]
                                update_logs(x)

                                if"No route to host" in x:
                                    error_type =  "DVR_NOT_ON_THE_SAME_NETWORK"
                                
                                if "404 Not Found" in x:
                                    error_type = "WRONG_URL"
                                    
                                if "401 Unauthorized" in x:
                                    error_type = "UNAUTHORISED"

                                if len(success_channels) > 0:
                                    self.all_dvr[i]["is_checked"] = True
                                    update_logs("dvr check complete")
                                    with open('persampling_status.json', 'r') as openfile:
                                        current_file = json.load(openfile)

                                    current_file["DVRInfoList"] = self.all_dvr

                                    json_to_be_written = json.dumps(current_file, indent = 4)

                                    with open("persampling_status.json", "w") as outfile:
                                        outfile.write(json_to_be_written)

                                    break

                                else:

                                    if len(faild_channels) > 1:
                                        
                                        try:
                                            try:
                                                headers = {

                                                    'ApplicationId': '6eec1c22-26f4-4180-ae7f-f054de6d98d1', 
                                                    'SessionToken': '0x01',
                                                    'UserId': '634ea700-cbfe-4c99-b0b2-3b24a26f2d4a', 
                                                    'CustId': '3226ab2e-c8ea-40ac-9c18-449155fef756',
                                                    'MessageId': "test"
                                                }
                                                data = {
                                                    "ROID": self.roid,
                                                    "Message": x,
                                                    "MessageType": error_type
                                                    }

                                                end_point = all_api_url["dvr_access_error_message"]

                                                response = requests.post(end_point, headers=headers, data=data)
                                                print(data)
                                            except:
                                                pass

                                            self.all_dvr[i]["is_checked"] = True
                                            update_logs("dvr check complete")
                                            with open('persampling_status.json', 'r') as openfile:
                                                current_file = json.load(openfile)

                                            current_file["DVRInfoList"] = self.all_dvr

                                            json_to_be_written = json.dumps(current_file, indent = 4)

                                            with open("persampling_status.json", "w") as outfile:
                                                outfile.write(json_to_be_written)

                                        except:
                                            pass

                                        break

                        else:
                            success_channels.append(channel)
                            name = "cam{}".format(len(self.all_camera_details))
                            cv2.imwrite(name + ".jpg", frame)
                            self.upload_images(url, name, name + ".jpg")
                            self.all_camera_details.append(name)

                            with open('persampling_status.json', 'r') as openfile:
                                current_file = json.load(openfile)

                            current_file["all_camer_current"] = self.all_camera_details

                            json_to_be_written = json.dumps(current_file, indent = 4)

                            with open("persampling_status.json", "w") as outfile:
                                outfile.write(json_to_be_written)

                            if channel == 16:
                                self.all_dvr[i]["is_checked"] = True
                                update_logs("maximum limit for channel reached")
                                update_logs("dvr check complete")
                                            
                                with open('persampling_status.json', 'r') as openfile:
                                    current_file = json.load(openfile)

                                current_file["DVRInfoList"] = self.all_dvr

                                json_to_be_written = json.dumps(current_file, indent = 4)

                                with open("persampling_status.json", "w") as outfile:
                                    outfile.write(json_to_be_written)

                                break
                        
                        channel += 1
                        cap.release()
                else:
                    
                    time.sleep(2)
                    url = dvr_url
                    update_logs("making source " + url)
                    cap = cv2.VideoCapture(url)
                    update_logs("source successful")
                    for z in range(0,10):
                        ret, frame = cap.read()

                    if not ret:

                        try:
                            is_pingable = False

                            if "linux" in sys.platform:
                                res = os.system("ping -c 1 " + self.all_dvr[i]["DVR_IP"])

                                #and then check the response...
                                if res == 0:
                                    is_pingable = True
                                    update_logs("ip is pingable")
                                    
                                else:
                                    update_logs("ip is not pingable")

                            else:
                                res = os.popen("ping " + self.all_dvr[i]["DVR_IP"]).read()
                                print("############")
                                #print(res)
                                res = str(res)
                                #print(type(res))

                                if "ms TTL=" in res:
                                    is_pingable = True
                                    update_logs("ip is pingable")
                                    update_logs(res)

                                else:
                                    update_logs("ip is not pingable")
                                    update_logs(res)
                                    
                            if is_pingable:

                                process = (
                                    ffmpeg
                                    .input(url)
                                    .output('-', format='h264')
                                    .run(capture_stdout=True, capture_stderr=True)
                                )
                            else:
                                try:
                                    try:
                                        headers = {

                                            'ApplicationId': '6eec1c22-26f4-4180-ae7f-f054de6d98d1', 
                                            'SessionToken': '0x01',
                                            'UserId': '634ea700-cbfe-4c99-b0b2-3b24a26f2d4a', 
                                            'CustId': '3226ab2e-c8ea-40ac-9c18-449155fef756',
                                            'MessageId': "test"
                                        }
                                        data = {
                                            "ROID": self.roid,
                                            "Message": "DVR_NOT_ON_THE_SAME_NETWORK",
                                            "MessageType": "DVR_NOT_ON_THE_SAME_NETWORK"
                                            }

                                        end_point = all_api_url["dvr_access_error_message"]

                                        response = requests.post(end_point, headers=headers, data=data)
                                        print(data)
                                    except:
                                        pass

                                    self.all_dvr[i]["is_checked"] = True
                                    update_logs("dvr check complete")
                                    with open('persampling_status.json', 'r') as openfile:
                                        current_file = json.load(openfile)

                                    current_file["DVRInfoList"] = self.all_dvr

                                    json_to_be_written = json.dumps(current_file, indent = 4)

                                    with open("persampling_status.json", "w") as outfile:
                                        outfile.write(json_to_be_written)

                                except:
                                    pass

                                break

                        except Exception as e:
                            error_type = "UNKNOWN"
                            a = e.stderr.decode('utf8')
                            x = a.split("\n")[-3] + "\n"+a.split("\n")[-2]
                            update_logs(x)
                            if"No route to host" in x:
                                error_type =  "DVR_NOT_ON_THE_SAME_NETWORK"
                            
                            if "404 Not Found" in x:
                                error_type = "WRONG_URL"
                                
                            if "401 Unauthorized" in x:
                                error_type = "UNAUTHORISED"

                            try:
                                try:
                                    headers = {

                                        'ApplicationId': '6eec1c22-26f4-4180-ae7f-f054de6d98d1', 
                                        'SessionToken': '0x01',
                                        'UserId': '634ea700-cbfe-4c99-b0b2-3b24a26f2d4a', 
                                        'CustId': '3226ab2e-c8ea-40ac-9c18-449155fef756',
                                        'MessageId': "test"
                                    }
                                    data = {
                                        "ROID": self.roid,
                                        "Message": x,
                                        "MessageType": error_type
                                        }

                                    end_point = all_api_url["dvr_access_error_message"]

                                    response = requests.post(end_point, headers=headers, data=data)
                                    print(data)

                                except:
                                    pass
                                
                                self.all_dvr[i]["is_checked"] = True
                                update_logs("camera checked")
                                with open('persampling_status.json', 'r') as openfile:
                                    current_file = json.load(openfile)

                                current_file["DVRInfoList"] = self.all_dvr

                                json_to_be_written = json.dumps(current_file, indent = 4)

                                with open("persampling_status.json", "w") as outfile:
                                    outfile.write(json_to_be_written)

                            except:
                                pass
                    
                    
                    else:
                       
                        name = "cam{}".format(len(self.all_camera_details))
                        cv2.imwrite(name + ".jpg", frame)
                        self.upload_images(url, name, name + ".jpg")
                        self.all_camera_details.append(name)
                        self.all_dvr[i]["is_checked"] = True
                        with open('persampling_status.json', 'r') as openfile:
                            current_file = json.load(openfile)

                        current_file["all_camer_current"] = self.all_camera_details
                        current_file["DVRInfoList"] = self.all_dvr

                        json_to_be_written = json.dumps(current_file, indent = 4)

                        with open("persampling_status.json", "w") as outfile:
                            outfile.write(json_to_be_written)


    def run_once(self):
        self.pull_pre_sampling_config()
        self.check_dvrs()
        self.check_cameras()

    def run(self):
        send_log_timer = time.time()
        app_update_check_timer = time.time()

        while True:
            if time.time()- app_update_check_timer > 3600:
                check_app_update(self.roid)
                app_update_check_timer = time.time()

            if time.time() - send_log_timer > 600:
                send_logs(self.roid)
                send_log_timer = time.time()

            self.check_dvrs()
            self.check_cameras()
            self.pull_pre_sampling_config()

            flag = self.pull_system_config()
            if flag:
                break
            
            time.sleep(10)


if __name__ == "__main__":
    print("new app")
    if not os.path.exists("api_detais.json"):
        update_logs("checking for api detais")
        if parse_api_detail_from_presampling():
            update_logs("api detais present in presampling")
            load_api_details()
            update_logs("loaded api details")

        else:
            update_logs("api details not present in presampling")
            while True:
                update_logs("pulling api details from dashbord")
                flag = pull_api_details()
                if flag:
                    break
            load_api_details()
    else:
        update_logs("loading api details")
        load_api_details()

    if not os.path.exists("system_config.json"):
        x =  pre_sampling_app()
        x.run()

        with open('system_config.json', 'r') as openfile:
            json_object = json.load(openfile)

        json_object["lastsynctime"] = 0
        json_object["is_over_ride"] = True
        json_object["start_hour"] = 0
        json_object["start_min"] = 1

        json_to_be_written = json.dumps(json_object, indent = 4)

        with open("system_config.json", "w") as outfile:
            outfile.write(json_to_be_written)

        
    
    while True:
        try:
            update_logs(sampling_app_version)
            a = sampling()

        except Exception as e: 
            os.remove("camera_config.json")
            os.remove("system_config.json")
            os.remove("videos_to_upload.json")
            update_logs("app crashed")
            update_logs(str(e))
            sys.exit()

        if a.over_ride_camera_config or a.over_ride_system_config:
            a.run_over_ride()

        if a.over_ride_system_config == False and a.over_ride_camera_config == False:
            flag = a.run()
            
            if not flag:
                continue

            a.run_over_ride()



            




